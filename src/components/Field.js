import React from "react";
import LanguageContext from "../contexts/LanguageContext";

class Field extends React.Component {
  static contextType = LanguageContext;

  render() {
    // const text = this.context === "english" ? "Name" : "Naam";
    // if you don't want to use a Consumer, uncomment line 8 and
    // replace lines 15 to 17 with {text}

    return (
      <div className="ui field">
        <label>
          <LanguageContext.Consumer>
            {value => (value === "english" ? "Name" : "Naam")}
          </LanguageContext.Consumer>
        </label>
        <input />
      </div>
    );
  }
}

export default Field;
